function checkValidation() {
  //  check = true;
  // Kiem tra rỗng
  var check =
    kiemTraRong("firstName", "error_firstName") &
    kiemTraRong("lastName", "error_lastName") &
    kiemTraRong("password", "error_password") &
    kiemTraRong("email", "error_email") &
    // kiemTraRong("phone", "error_phone") &
    kiemTraRong("confirmPassword", "error_confirmPassword");
  check &=
    kiemTraAllLaChu("#firstName", "#error_firstName_all_leter") &
    kiemTraAllLaChu("#lastName", "#error_lastName_all_leter");
  check &= kiemTraAllLaSo("#phone", "#error_phone");
  check &= kiemTraEmail("#email", "#error_email");
  check &= kiemTraDoDai("#password", "#error_password_min_max_length");
  check &= kiemTraGiaTriPas(
    "#password",
    "#confirmPassword",
    "#error_confirmPassword"
  );
  // Vì & chỉ trả về 0 or 1 không phải false/true
  if (!check) {
    return false;
  }
  return true;
}
var kiemTraRong = (selectorValue, selectorError) => {
  var inputText = document.getElementById(selectorValue);
  var messageError = document.getElementById(selectorError);
  if (inputText.value.trim() == "") {
    messageError.innerHTML = inputText.name + " ko được để trống";
    messageError.style.display = "block";
    return false;
  } else {
    document.getElementById(selectorError).innerHTML = "";
    messageError.style.display = "none";
    return true;
  }
};
var kiemTraAllLaChu = (selectorValue, selectorError) => {
  var pattern = /^[A-Za-z]+$/;
  var text = document.querySelector(selectorValue);
  var mesError = document.querySelector(selectorError);
  if (pattern.test(text.value)) {
    // hợp lệ
    mesError.innerHTML = "";
    mesError.style.display = "none";
    return true;
  } else {
    mesError.innerHTML = text.name + " phải là chữ !";
    mesError.style.display = "block";
    return false;
  }
};
var kiemTraAllLaSo = (selectorValue, selectorError) => {
  var pattern = /^[0-9]+$/;
  var text = document.querySelector(selectorValue);
  var mesError = document.querySelector(selectorError);
  if (pattern.test(text.value)) {
    // hợp lệ
    mesError.innerHTML = "";
    mesError.style.display = "none";
    return true;
  } else {
    mesError.innerHTML = text.name + " phải là số !";
    mesError.style.display = "block";
    return false;
  }
};
var kiemTraEmail = (selectorValue, selectorError) => {
  var mailformat =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\ [[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  var text = document.querySelector(selectorValue);
  var mesError = document.querySelector(selectorError);
  if (mailformat.test(text.value)) {
    // hợp lệ
    mesError.innerHTML = "";
    mesError.style.display = "none";
    return true;
  } else {
    mesError.innerHTML = text.name + " ko hợp lệ !";
    mesError.style.display = "block";
    return false;
  }
};
var kiemTraDoDai = (selectorValue, selectorError) => {
  var password = document.querySelector(selectorValue);
  var mesError = document.querySelector(selectorError);
  if (
    password.value.length >= password.minLength &&
    password.value.length <= password.maxLength
  ) {
    mesError.innerHTML = "";
    mesError.style.display = "none";
    return true;
  } else {
    mesError.innerHTML =
      password.name +
      " từ " +
      password.minLength +
      " đến " +
      password.maxLength +
      " ký tự!";
    mesError.style.display = "block";
    return false;
  }
};
var kiemTraGiaTriPas = (selectorPass1, selectorPass2, selectorError) => {
  var pas_val1 = document.querySelector(selectorPass1);
  var pas_val2 = document.querySelector(selectorPass2);
  var mesError = document.querySelector(selectorError);
  if (pas_val1.value === pas_val2.value) {
    mesError.innerHTML = "";
    mesError.style.display = "none";
  } else {
    mesError.innerHTML = pas_val2.name + " sai !";
    mesError.style.display = "block";
  }
};
document.getElementById("firstName").onblur = checkValidation;
document.getElementById("lastName").onblur = checkValidation;
document.querySelector("#btnDangKy").onclick = checkValidation;
